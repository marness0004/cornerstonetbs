using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObject : MonoBehaviour
{
    public GameObject targetObject;
    public float distanceLimit;

    private float targetObjectHeight;
    private float distanceCalculated;
    private bool orderToMove = false;
    private Vector3 endVector;

    Vector3 targetObjectNextPosition;
    RaycastHit detect;
    RaycastHit hit;


    void Start()
    {
        targetObjectNextPosition = targetObject.transform.position;
        targetObjectHeight = targetObject.GetComponent<MeshRenderer>().bounds.size.y;
        Debug.Log(distanceCalculated);
    }

    private void OnMouseDown()
    {
        Ray cam = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast (cam, out detect))
        {
            if(detect.transform.CompareTag("Unit"))
            {
                targetObject = detect.collider.GetComponent<GameObject>();
            }
        }
    }

    public void ClickToMove()
    {
        //Casts ray to location clicked
        Vector3 worldMousePosition = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 100f));

        Vector3 direction = worldMousePosition - Camera.main.transform.position;

        //creates new Vector from end point that is clicked
        endVector = hit.point + new Vector3(0f, targetObjectHeight / 2f, 0f);

        //Calculates the distance between the targets current position and the targeted position
        distanceCalculated = Vector3.Distance(targetObject.transform.position, endVector);
        Debug.Log(distanceCalculated);

        //Decides if point selected will be vaild                                   
        if(Physics.Raycast(Camera.main.transform.position, direction, out hit, 100f))
        {
           Debug.DrawLine(Camera.main.transform.position, hit.point, Color.green, 0.5f);
           targetObjectNextPosition = hit.point + new Vector3(0f, targetObjectHeight / 2f, 0f);

           orderToMove = true;

           //targetObject.transform.position = targetObjectNextPosition;
        } else {
           Debug.DrawLine(Camera.main.transform.position, worldMousePosition, Color.red, 0.5f);
        }
    }

    public void Update()
    {
        if(orderToMove == true) //&& distanceCalculated <= distanceLimit)
        {
            Debug.DrawLine(targetObject.transform.position, targetObjectNextPosition, Color.blue, 0.5f);
            targetObject.transform.position = Vector3.MoveTowards(targetObject.transform.position, targetObjectNextPosition, 5f * Time.deltaTime);
            //distanceCalculated = 0;
        }
         if (Vector3.Distance(targetObject.transform.position, targetObjectNextPosition) < 0.1f)
        { 
            orderToMove = false;
            distanceCalculated = 0;
        }
    }
}
