using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectObject : MonoBehaviour
{
    public Color startColor;
    public Color selectedColor;
    public Color clickSelected;
    public int maxMovement;
    public int movementCost;
    public int currentMovementsNumber;
    public bool canMove = false;
    public ActiveObject cameraObject;

    private Renderer theRenderer;
    private bool isNotSelected = true;

    void Start()
    {
        cameraObject = Camera.main.gameObject.GetComponent<ActiveObject>();
        theRenderer = GetComponent<Renderer>();
        ResetMovementPoints();
    }

    private void ResetMovementPoints()
    {
        currentMovementsNumber = maxMovement;
    }

    void OnMouseEnter()
    {
        theRenderer.material.SetColor("_Color", selectedColor);
        isNotSelected = false;
        //Debug.Log("MouseEnter");
    }

    void OnMouseExit()
    {
        if (isNotSelected == false)
        {
            theRenderer.material.SetColor("_Color", startColor);
            isNotSelected = true;
            //Debug.Log("MouseExit");
        }
    }

    public void OnMouseDown()
    {
        if (Input.GetMouseButtonDown(0))
        {
            cameraObject.targetObject = this.gameObject;
            theRenderer.material.SetColor("_Color", clickSelected);
            //Debug.Log("MouseDown");
            if (currentMovementsNumber - movementCost < 0)
            {
                Debug.LogError($"Not enough movement points {currentMovementsNumber} to move {movementCost}.");
                canMove = false;
            }
            else
            {
                Debug.Log(currentMovementsNumber);
                canMove = true;
            }
            cameraObject.UnitHandle();
        }
    }

    public void MoveCalculation()
    {
        currentMovementsNumber -= movementCost;
        Debug.Log(currentMovementsNumber);
    }

    public void WaitTurn()
    {
        ResetMovementPoints();
    }
}
