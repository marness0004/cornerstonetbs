using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActiveObject : MonoBehaviour
{
    public Text UnitUIText;
    public Text MovementPoints;
    public Button m_MoveUp, m_MoveDown, m_MoveRight, m_MoveLeft;
    public GameObject fatherButton;
    public GameObject targetObject;
    public GameObject buildUI;
    public bool isSelected = false;

    private int moveUpDistance = 2;
    private int moveDownDistance = 2;
    private int moveLeftDistance = 2;
    private int moveRightDistance = 2;
    private Renderer theRenderer;
    void Start()
    {
        theRenderer = GetComponent<Renderer>();
        fatherButton = GameObject.Find("ButtonFather");
        UnitUIText.gameObject.SetActive(false);
        buildUI.gameObject.SetActive(false);
        fatherButton.SetActive(false);
        m_MoveUp.onClick.AddListener(MUP);
        m_MoveDown.onClick.AddListener(MDOWN);
        m_MoveRight.onClick.AddListener(MRIGHT);
        m_MoveLeft.onClick.AddListener(MLEFT);
    }


    public void Update()
    {
        if (isSelected == true)
        {
            fatherButton.SetActive(true);
            UnitUIText.gameObject.SetActive(true);
            MovementPoints.text = "Movement Points Remaining: " + targetObject.gameObject.GetComponent<SelectObject>().currentMovementsNumber;

            if (targetObject.gameObject.CompareTag("Settler"))
            {
                buildUI.gameObject.SetActive(true);
            }
            else
            {
                buildUI.gameObject.SetActive(false);
            }
        }
        if (isSelected == true && Input.GetMouseButtonDown(1))
        {
            fatherButton.SetActive(false);
            UnitUIText.gameObject.SetActive(false);
            buildUI.gameObject.SetActive(false);
        }
    }

    public void UnitHandle()
    {
        if (targetObject.gameObject.GetComponent<SelectObject>().canMove == true)
        {
            isSelected = true;
        }

        if (targetObject.gameObject.CompareTag("Settler"))
        {
            //Debug.Log("Settler Tag");
            UnitUIText.gameObject.SetActive(true);
            UnitUIText.text = "Settler";
        }
        if (targetObject.gameObject.CompareTag("Unit1"))
        {
            //Debug.Log("Unit1 Tag");
            //targetObject = 
            UnitUIText.gameObject.SetActive(true);
            UnitUIText.text = "Unit1";
        }
    }
    

    public void MUP()
    {
        targetObject.transform.Translate(Vector3.forward * moveUpDistance);
        isSelected = false;
        fatherButton.SetActive(false);
        UnitUIText.gameObject.SetActive(false);
        buildUI.gameObject.SetActive(false);
        targetObject.gameObject.GetComponent<SelectObject>().MoveCalculation();
    }

    public void MDOWN()
    {
        targetObject.transform.Translate(Vector3.down * moveDownDistance);
        isSelected = false;
        fatherButton.SetActive(false);
        UnitUIText.gameObject.SetActive(false);
        buildUI.gameObject.SetActive(false);
        targetObject.gameObject.GetComponent<SelectObject>().MoveCalculation();
    }

    public void MRIGHT()
    {
        targetObject.transform.Translate(Vector3.right * moveRightDistance);
        isSelected = false;
        fatherButton.SetActive(false);
        UnitUIText.gameObject.SetActive(false);
        buildUI.gameObject.SetActive(false);
        targetObject.gameObject.GetComponent<SelectObject>().MoveCalculation();
    }

    public void MLEFT()
    {
        targetObject.transform.Translate(Vector3.left * moveLeftDistance);
        isSelected = false;
        fatherButton.SetActive(false);
        UnitUIText.gameObject.SetActive(false);
        buildUI.gameObject.SetActive(false);
        targetObject.gameObject.GetComponent<SelectObject>().MoveCalculation();
    }

    public void PressedToSettleCity()
    {
        fatherButton.SetActive(false);
        UnitUIText.gameObject.SetActive(false);
        buildUI.gameObject.SetActive(false);
        Destroy(targetObject.gameObject);
        targetObject = null;
    }
}

/*
    public GameObject UIUnitText;
    public GameObject moveUp;
    public GameObject moveDown;
    public GameObject moveLeft;
    public GameObject moveRight;
    public GameObject targetObject;
    public SelectObject Unit1;
    public float moveUpDistance;
    public float moveDownDistance;
    public float moveLeftDistance;
    public float moveRightDistance;
    public Text unitText;
    public bool isSelected = false;

    private SelectObject SetUnitUIText;
    private Renderer theRenderer;
    void Start()
    {
        theRenderer = GetComponent<Renderer>();
        //UIUnitText = Text.Find("UnitTextUI");
        moveUp = GameObject.Find("UpMovement");
        moveDown = GameObject.Find("BottomMovement");
        moveLeft = GameObject.Find("LeftMovement");
        moveRight = GameObject.Find("RightMovement");
        //#####//
        moveUp.SetActive(false);
        moveDown.SetActive(false);
        moveLeft.SetActive(false);
        moveRight.SetActive(false);
    }


    public void Update()
    {
        if (isSelected == true)
        {
            //unitText = SetUnitUIText.unitText;
            moveUp.SetActive(true);
            moveDown.SetActive(true);
            moveLeft.SetActive(true);
            moveRight.SetActive(true);
            //UIUnitText = unitText;
        }
    }

    public void Unit1Handle()
    {
        isSelected = true;
    }
    

    public void MoveUpHandle()
    {
        targetObject.transform.Translate(Vector3.forward * moveUpDistance);
        isSelected = false;
        moveUp.SetActive(false);
        moveDown.SetActive(false);
        moveLeft.SetActive(false);
        moveRight.SetActive(false);
    }

    public void MoveDownHandle()
    {
        targetObject.transform.Translate(Vector3.down * moveDownDistance);
        isSelected = false;
        moveUp.SetActive(false);
        moveDown.SetActive(false);
        moveLeft.SetActive(false);
        moveRight.SetActive(false);
    }

    public void MoveLeftHandle()
    {
        targetObject.transform.Translate(Vector3.left * moveLeftDistance);
        isSelected = false;
        moveUp.SetActive(false);
        moveDown.SetActive(false);
        moveLeft.SetActive(false);
        moveRight.SetActive(false);
    }

    public void MoveRightHandle()
    {
        targetObject.transform.Translate(Vector3.right * moveRightDistance);
        isSelected = false;
        moveUp.SetActive(false);
        moveDown.SetActive(false);
        moveLeft.SetActive(false);
        moveRight.SetActive(false);
    } 
*/
