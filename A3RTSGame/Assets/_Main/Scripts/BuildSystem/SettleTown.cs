using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettleTown : MonoBehaviour
{
    public GameObject townPreFab;
    public ActiveObject settlerObject;

    public void Start()
    {
        settlerObject = Camera.main.gameObject.GetComponent<ActiveObject>();
    }

    public void HandleSettleTown()
    {
        Instantiate(townPreFab, settlerObject.targetObject.transform.position, settlerObject.targetObject.transform.rotation);
        settlerObject.PressedToSettleCity();
    }
}
