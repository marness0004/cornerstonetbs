using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class TurnManger : MonoBehaviour
{
    public UnityEvent OnBlockPlayerInput, onUnblockPlayerInput;
    public Text TurnCounter;

    private int counter = 1;

    public void Start()
    {
        TurnCounter.text = "Turn: 1";
    }

    public void NextTurn()
    {
        Debug.Log("Tick Tok..");
        OnBlockPlayerInput?.Invoke();
        foreach (SelectObject unit in FindObjectsOfType<SelectObject>())
        {
            unit.WaitTurn();
            Debug.Log($"Unit {unit.name} is waiting");
        }
        Debug.Log("New turn ready!");
        onUnblockPlayerInput.Invoke();
        counter++;
        TurnCounter.text = "Turn: " + counter.ToString();
    }
}
