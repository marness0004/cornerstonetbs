using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighlightAndSelect : MonoBehaviour
{
    public Color startColor;
    public Color selectedColor;
    public Color clickSelected;
    public GameObject unitMoveOverlay;
    public GameObject thisObject;
    public MoveObject cameraClicker;

    private bool isNotSelected = true;
    private bool commandTheeToMove = false;
    private Renderer theRenderer;

    private void Start()
    {
        theRenderer = GetComponent<Renderer>();    
    }

    void OnMouseEnter()
    {
        theRenderer.material.SetColor("_Color", selectedColor );
        isNotSelected = false;
}
    void OnMouseExit()
    {
        if (isNotSelected == false)
        {
            theRenderer.material.SetColor("_Color", startColor);
            isNotSelected = true;
        }
    }
    
    public void OnMouseUp()
    {
        if (Input.GetMouseButtonUp(0) && commandTheeToMove == false)
        {
            theRenderer.material.SetColor("_Color", clickSelected);
            isNotSelected = true;
            commandTheeToMove = true;
        }
    }

    public void Update()
    {
        if (Input.GetMouseButtonDown(0) && commandTheeToMove == true)
        {
            cameraClicker.ClickToMove();
            theRenderer.material.SetColor("_Color", startColor);
            commandTheeToMove = false;
            isNotSelected = true;
        }
    }
}
